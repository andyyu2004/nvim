set expandtab
set number
set relativenumber
set undofile
set splitright
set cursorline
set smartcase
set noshowmode
set wrap
set noswapfile
set tabstop=4
set shiftwidth=4
set mouse=a
set updatetime=350
set signcolumn=yes
set completeopt=menu,menuone,noinsert

syntax enable
filetype plugin indent on

let g:haskell_classic_highlighting=1
