inoremap fd <esc>
vnoremap fd <esc>
inoremap <leader><Tab> <C-^>
nnoremap <leader>rl :so $NVIM/init.vim<CR>
nnoremap <C-s> :w<CR>
map <leader><Tab> <C-^>

nnoremap <silent> <leader>s <cmd>Telescope current_buffer_fuzzy_find<cr>
nnoremap <silent> <leader>o <cmd>Telescope find_files<cr>
nnoremap <silent> <leader>g <cmd>Telescope live_grep<cr>
nnoremap <silent> <leader>d <cmd>Telescope buffers<cr>
" nnoremap <silent> <leader>f :Files <C-R>=expand('%:h')<CR><CR>
" nnoremap <silent> <leader>y <cmd>Telescope neoclbuffersip<cr>
nnoremap <silent> <leader>ts <cmd>TSHighlightCapturesUnderCursor<CR>
nnoremap <leader>e :Explore<CR>

nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>

map  <C-_> <Plug>kommentary_line_default
vmap <C-_> <Plug>kommentary_visual_default

inoremap <S-Tab> <C-d>
