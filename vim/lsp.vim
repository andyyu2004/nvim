nnoremap <silent> K          <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gt         <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr         <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> <leader>qo <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> <leader>rn <cmd>lua vim.lsp.buf.rename()<CR>
nnoremap <silent> gd         <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gi         <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> gD         <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <leader>.  :CodeActionMenu<CR>

augroup format
  autocmd BufWritePre *.lua,*.rs,*.hs lua vim.lsp.buf.formatting_sync(nil, 200)
augroup end

autocmd CursorHold * lua vim.diagnostic.open_float({focusable=false})
