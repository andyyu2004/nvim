set background=dark

colorscheme solarized-flat

highlight StatusLine guibg=#888888

highlight! EndOfBuffer ctermbg=bg ctermfg=bg guibg=bg guifg=bg

let g:netrw_banner=0

if exists('+termguicolors')
   " let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
   " let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
   set termguicolors
endif


set guifont=Code\ New\ Roman:h10.5
let g:neovide_cursor_animation_length=0
