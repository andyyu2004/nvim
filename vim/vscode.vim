nnoremap <silent> <leader>g      <Cmd>call VSCodeNotify('vscode-fzf.rg')<CR>
nnoremap <silent> <leader>o      <Cmd>call VSCodeNotify('workbench.action.quickOpen')<CR>
nnoremap <silent> <leader>p      <Cmd>call VSCodeNotify('workbench.action.showCommands')<CR>
nnoremap <silent> <leader>w      <Cmd>call VSCodeNotify('workbench.action.showAllSymbols')<CR>
nnoremap <silent> <leader>s      <Cmd>call VSCodeNotify('fuzzySearch.activeTextEditor')<CR>
nnoremap <silent> <leader>f      <Cmd>call VSCodeNotify('quickopen-currentdir.quick-open-current-dir')<CR>
nnoremap <silent> <leader>e      <Cmd>call VSCodeNotify('workbench.view.explorer')<CR>
nnoremap <silent> <leader>rl     <Cmd>call VSCodeNotify('workbench.action.reloadWindow')<CR>
nnoremap <silent> <leader>rn     <Cmd>call VSCodeNotify('editor.action.rename')<CR>
nnoremap <silent> <leader>.      <Cmd>call VSCodeNotify('keyboard-quickfix.openQuickFix')<CR>
nnoremap <silent> <leader>q      <Cmd>call VSCodeNotify('workbench.action.gotoSymbol')<CR>
nnoremap <silent> <leader>b      <Cmd>call VSCodeNotify('editor.debug.action.toggleBreakpoint')<CR>
nnoremap <silent> <leader>_      <Cmd>call VSCodeNotify('editor.action.startFindReplaceAction')<CR>
nnoremap <silent> <leader><tab>  <Cmd>call VSCodeNotify('extension.goto-previous-buffer')<CR>
nnoremap <silent> <leader>t      <Cmd>call VSCodeNotify('workbench.action.terminal.toggleTerminal')<CR>
nnoremap <silent> <leader>d      <Cmd>call VSCodeNotify('open-buffers.openBuffer')<CR>
nnoremap <silent> <leader>l      <Cmd>call VSCodeNotify('workbench.actions.view.toggleProblems')<CR>
nnoremap <silent> <leader>ts     <Cmd>call VSCodeNotify('editor.action.inspectTMScopes')<CR>


nnoremap <silent> gt <Cmd>call VSCodeNotify('editor.action.goToTypeDefinition')<CR>
nnoremap <silent> gr <Cmd>call VSCodeNotify('editor.action.goToReferences')<CR>
nnoremap <silent> gi <Cmd>call VSCodeNotify('editor.action.goToImplementation')<CR>

xmap gc  <Plug>VSCodeCommentary
nmap gc  <Plug>VSCodeCommentary
omap gc  <Plug>VSCodeCommentary
nmap gcc <Plug>VSCodeCommentaryLine

