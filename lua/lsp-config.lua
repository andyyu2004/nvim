local nvim_lsp = require'lspconfig'

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
 vim.lsp.diagnostic.on_publish_diagnostics, {
   underline = true,
   virtual_text = {
     spacing = 2,
     severity_limit = "Error",
   },
 }
)

local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

nvim_lsp.gopls.setup{}
nvim_lsp.bashls.setup{}
nvim_lsp.clangd.setup{}
nvim_lsp.dockerls.setup{}
nvim_lsp.graphql.setup{}
nvim_lsp.hls.setup{}
nvim_lsp.pyright.setup{}

nvim_lsp.rust_analyzer.setup {
    settings = {
        ["rust-analyzer"] = {
            procMacro = { enable = true },
            diagnostics = { disabled = {"unresolved-proc-macro"} },
        }
    }
}

nvim_lsp.vimls.setup{}
nvim_lsp.sumneko_lua.setup{}

vim.lsp.handlers['textDocument/references'] = require'lsputil.locations'.references_handler
vim.lsp.handlers['textDocument/declaration'] = require'lsputil.locations'.declaration_handler
vim.lsp.handlers['textDocument/typeDefinition'] = require'lsputil.locations'.typeDefinition_handler
vim.lsp.handlers['textDocument/implementation'] = require'lsputil.locations'.implementation_handler
vim.lsp.handlers['textDocument/documentSymbol'] = require'lsputil.symbols'.document_handler
vim.lsp.handlers['workspace/symbol'] = require'lsputil.symbols'.workspace_handler

