call plug#begin(stdpath('data') . '/plugged')
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'chaoren/vim-wordmotion'
Plug 'PeterRincker/vim-argumentative'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'nvim-treesitter/playground'

if exists('g:vscode')
    source $NVIM/vscode.vim
else

    Plug 'rhysd/committia.vim'
    Plug 'airblade/vim-rooter'
    Plug 'andyyu2004/vim-slash'

    Plug 'jiangmiao/auto-pairs'

    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'

    Plug 'andyyu2004/nvim-solarized-lua'

    Plug 'APZelos/blamer.nvim'

    Plug 'b3nj5m1n/kommentary'

    Plug 'neovimhaskell/haskell-vim'

    Plug 'neovim/nvim-lspconfig'

    Plug 'RishabhRD/popfix'
    Plug 'RishabhRD/nvim-lsputils'

    " Plug 'weilbith/nvim-code-action-menu'
    " FIXME a fork that fixes an annoying problem for now
    Plug 'filtsin/nvim-code-action-menu'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-lua/popup.nvim'

    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'folke/trouble.nvim'

    Plug 'onsails/lspkind-nvim'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/cmp-buffer'
    Plug 'hrsh7th/cmp-path'
    Plug 'hrsh7th/cmp-cmdline'
    Plug 'hrsh7th/nvim-cmp'
end
call plug#end()
