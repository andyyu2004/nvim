let $NVIM = '$HOME/.config/nvim/'

source $NVIM/vim/plug.vim
source $NVIM/vim/common.vim

if exists('g:vscode')
    source $NVIM/vim/vscode.vim
else
    source $NVIM/vim/bindings.vim
    source $NVIM/vim/config.vim
    source $NVIM/vim/lsp.vim
    source $NVIM/vim/theme.vim

    lua require("telescope-config")
    lua require("treesitter-config")
    lua require("cmp-config")
    lua require("lsp-config")
end
