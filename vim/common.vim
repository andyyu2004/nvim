noremap <Tab> >>_
vnoremap <Tab> >gv
nnoremap <S-Tab> <<_
vnoremap <S-Tab> <gv

nnoremap <enter> o<esc>

map <space> <leader>
 
set clipboard=unnamedplus

augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank { higroup='IncSearch', timeout=200 }
augroup end
